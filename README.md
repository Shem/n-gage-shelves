# N-Gage Shelves Version 1

Project to host .f3d and .stl files for my n-gage shelf design

# Contents

.stl file ready for printing with 5 shelves
.f3d file for Autodesk Fusion 360

# Specifications

Each shelf measures 97mm width x 16mm height x 136mm deep. This size allows the game box to slip in smoothly without force, yet holds the game firmly with friction, even when held vertical (though don't shake it...). The game sticks out by approximately 1mm or less, for looks and ease of access. The top shelf has a hole to show off the game, and also for easy removal. Games in lower shelves can be removed by pushing the back or easing forward from the sides.

The five-shelf .stl measures 101mm width x 92mm height x 138mm depth 

# Stacking

Stacking in V1 should be done in the autodesk software by tiling the Midshelf component and aligning the additonal shelves with the bottom/top shelf components. If you don't have Autodesk Fusion 360, feel free to [open an issue](https://gitlab.com/Shem/n-gage-shelves/-/issues/new) with the number of shelves you'd like and I can whip up an .stl for you.

I plan to update the design with physically-stackable shelves vertically and horizontally if possible. This can still be done in software easily though!

# Print Settings

The shelves are very forgiving, so these are just what I used with great success.

Model: Prusa i3 MK2
Layer height: 0.4mm
infill: 100% (recommended due to the overall thinness)
Supports: Yes, ZigZag
Adhesion: Brim

Be sure to rotate the model onto its back and print vertically for efficiency and to ensure the shelf dimensions are consistent and accurate, also, to avoid a nightmare amount of supports to remove:

![img](https://i.imgur.com/Y3KSXgo.png)

